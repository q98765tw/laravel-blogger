## Setup

```
$ composer install
```

## Copy ENV File

```
$ cp .env.example .env
```

## Application Key

```
$ php artisan key:generate
```

## Environment

```
$ vim .env

# modify DB parameters
DB_DATABASE={your db name}
DB_USERNAME={your db user}
DB_PASSWORD={your db password}
```

## Database

```
$ php artisan migrate
```
## 開serve

```
$ php artisan serve
```
先到首頁右上角登入，註冊並登入，在 home 頁按前往網站，即到達小 blogger。
## 圖片
![blog_jpg](https://q98765tw.gitlab.io/blogger/images/laravel-blog.jpg)
![](https://i.imgur.com/CmxKR3f.png)